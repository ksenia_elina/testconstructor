class ChangeQuestions < ActiveRecord::Migration[5.2]
  def change
    change_table :Questions do |t|
      t.references :tests, index: true, foreign_key: true
      t.references :variants, index: true, foreign_key: true
      t.references :typesquestions, index: true, foreign_key: true
    end
  end
end
