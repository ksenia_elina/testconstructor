class CreateTests < ActiveRecord::Migration[5.2]
  def change
    create_table :tests do |t|
      t.string :name
      t.integer :id_of_course
      t.string :inf
      t.boolean :quest
      t.integer :type

      t.timestamps
    end
  end
end
