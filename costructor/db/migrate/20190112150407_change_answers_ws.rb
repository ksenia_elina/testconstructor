class ChangeAnswersWs < ActiveRecord::Migration[5.2]
  def change
    change_table :Answers_Ws do |t|
      t.references :questions, index: true, foreign_key: true
    end
  end
end
