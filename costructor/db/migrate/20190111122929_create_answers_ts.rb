class CreateAnswersTs < ActiveRecord::Migration[5.2]
  def change
    create_table :answers_ts do |t|
      t.string :name
      t.string :right_t

      t.timestamps
    end
  end
end
