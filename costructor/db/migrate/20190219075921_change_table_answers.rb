class ChangeTableAnswers < ActiveRecord::Migration[5.2]
  def change
    change_table :answers_ts do |t|
      t.references :tests, index: true, foreign_key: true
      t.references :answers_ws, index: true, foreign_key: true
      t.references :users, index: true, foreign_key: true
    end
    end
    change_column(:answers_ts , :right_t, :boolean)
end
