class ChangeUsers < ActiveRecord::Migration[5.2]
  def change
    change_table :users do |t|
      t.references :roles, index: true, foreign_key: true
    end
  end
end
