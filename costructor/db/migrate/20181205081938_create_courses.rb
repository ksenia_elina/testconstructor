class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :name
      t.string :inf
      t.integer :id_of_par

      t.timestamps
    end
  end
end
