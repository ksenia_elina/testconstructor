class ChangeAnswersW < ActiveRecord::Migration[5.2]
  def change
    rename_column :answers_ws, :questions_id, :question_id
  end
end
