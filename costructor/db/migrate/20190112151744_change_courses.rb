class ChangeCourses < ActiveRecord::Migration[5.2]
  def change
    change_table :courses do |t|
      t.references :courses, index: true, foreign_key: true
    end
  end
end
