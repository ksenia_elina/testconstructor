class TestsTypeTests < ActiveRecord::Migration[5.2]
  def change
    change_table :Tests do |t|
    t.references :types_tests, index: true, foreign_key: true
    end
  end
end
