class CreateTypesTests < ActiveRecord::Migration[5.2]
  def change
    create_table :types_tests do |t|
      t.string :name

      t.timestamps
    end
  end
end
