class ChangeQuestion < ActiveRecord::Migration[5.2]
  def change
    rename_column :questions, :tests_id, :test_id
  end
end
