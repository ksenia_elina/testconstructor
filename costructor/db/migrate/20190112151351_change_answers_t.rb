class ChangeAnswersT < ActiveRecord::Migration[5.2]
  def change
    change_table :Answers_Ts do |t|
      t.references :questions, index: true, foreign_key: true
    end
  end
end
