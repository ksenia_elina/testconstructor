class ChangeTest < ActiveRecord::Migration[5.2]
  def change
    change_table :tests do |t|
      t.rename :type, :type_of_test
    end
  end
end
