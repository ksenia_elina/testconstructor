class ChangeForiginKey < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :tests, :courses
    add_foreign_key :tests, :types_tests
    add_foreign_key :courses, :courses
    add_foreign_key :questions, :typesquestions
   
  end
end
