class ChangeTestUsers < ActiveRecord::Migration[5.2]
  def change
    change_table :users_tests do |t|
      t.references :users, index: true, foreign_key: true
      t.references :tests, index: true, foreign_key: true
    end
  end
end
