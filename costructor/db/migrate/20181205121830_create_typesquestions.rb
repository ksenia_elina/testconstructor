class CreateTypesquestions < ActiveRecord::Migration[5.2]
  def change
    create_table :typesquestions do |t|
      t.string :name

      t.timestamps
    end
  end
end
