class CreateAnswersWs < ActiveRecord::Migration[5.2]
  def change
    create_table :answers_ws do |t|
      t.string :name
      t.boolean :right

      t.timestamps
    end
  end
end
