# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_22_132533) do

  create_table "Tests", force: :cascade do |t|
    t.string "name"
    t.string "inf"
    t.boolean "quest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "types_tests_id"
    t.integer "courses_id"
    t.index ["courses_id"], name: "index_Tests_on_courses_id"
    t.index ["types_tests_id"], name: "index_Tests_on_types_tests_id"
  end

  create_table "answers_ts", force: :cascade do |t|
    t.string "name"
    t.boolean "right_t"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "questions_id"
    t.integer "tests_id"
    t.integer "answers_ws_id"
    t.integer "users_id"
    t.index ["answers_ws_id"], name: "index_answers_ts_on_answers_ws_id"
    t.index ["questions_id"], name: "index_Answers_Ts_on_questions_id"
    t.index ["tests_id"], name: "index_answers_ts_on_tests_id"
    t.index ["users_id"], name: "index_answers_ts_on_users_id"
  end

  create_table "answers_ws", force: :cascade do |t|
    t.string "name"
    t.boolean "right"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "question_id"
    t.index ["question_id"], name: "index_Answers_Ws_on_questions_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.string "inf"
    t.integer "id_of_par"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "courses_id"
    t.index ["courses_id"], name: "index_courses_on_courses_id"
  end

  create_table "questions", force: :cascade do |t|
    t.string "name"
    t.integer "type_of_question"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "test_id"
    t.integer "variants_id"
    t.integer "typesquestions_id"
    t.index ["test_id"], name: "index_Questions_on_tests_id"
    t.index ["typesquestions_id"], name: "index_Questions_on_typesquestions_id"
    t.index ["variants_id"], name: "index_Questions_on_variants_id"
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "types_tests", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "typesquestions", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "login"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "roles_id"
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["roles_id"], name: "index_users_on_roles_id"
  end

  create_table "users_tests", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "users_id"
    t.integer "tests_id"
    t.index ["tests_id"], name: "index_users_tests_on_tests_id"
    t.index ["users_id"], name: "index_users_tests_on_users_id"
  end

  create_table "variants", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
