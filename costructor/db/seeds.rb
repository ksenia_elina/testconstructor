# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user = User.new
user.email = 'test@example.com'
user.password = 'valid_password'
user.password_confirmation = 'valid_password'
user.role = :admin
user.save!

user = User.new
user.email = 'user@ex.com'
user.password = 'valid_password'
user.password_confirmation = 'valid_password'
user.role = :user
user.save!

item = Course.new
item.name = '1'
item.save!

item = TypesTest.new
item.name = 'with question'
item.save!

item = Typesquestion.new
item.name = 'with answer'
item.save!

item = Variant.new
item.name = '1'
item.save!
