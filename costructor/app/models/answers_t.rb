class AnswersT < ApplicationRecord
    belongs_to :question,required: true,  :foreign_key => "questions_id"
    belongs_to :answers_w,required: false,  :foreign_key => "answers_ws_id"
    belongs_to :test,required: false,  :foreign_key => "tests_id"
    belongs_to :user,required: true,  :foreign_key => "users_id"
end
