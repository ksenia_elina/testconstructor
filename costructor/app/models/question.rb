class Question < ApplicationRecord
    has_many :answers_t
    has_many :answers_ws, inverse_of: :questions
    belongs_to :typesquestion,required: true,  :foreign_key => "typesquestions_id"
    belongs_to :variant,required: true,  :foreign_key => "variants_id"
    belongs_to :test,required: true,  :foreign_key => "test_id"
    validates_presence_of :test
    accepts_nested_attributes_for :answers_ws
end
