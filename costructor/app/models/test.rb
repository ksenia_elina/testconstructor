class Test < ApplicationRecord
    belongs_to :types_test,required: true,  :foreign_key => "types_tests_id"
    belongs_to :course,required: true,  :foreign_key => "courses_id"
    has_many :questions, inverse_of: :test
    accepts_nested_attributes_for :questions
    has_many :users_test
    has_many :answers_t
end
