class UsersTestsController < ApplicationController
  before_action :set_users_test, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  #before_action :require_admin
  # GET /users_tests
  # GET /users_tests.json
  def index
    @users_tests = UsersTest.all
  end

  # GET /users_tests/1
  # GET /users_tests/1.json
  def show
  end

  # GET /users_tests/new
  def new
    @users_test = UsersTest.new
  end

  # GET /users_tests/1/edit
  def edit
  end

  # POST /users_tests
  # POST /users_tests.json
  def create
    @users_test = UsersTest.new(users_test_params)

    respond_to do |format|
      if @users_test.save
        format.html { redirect_to @users_test, notice: 'Users test was successfully created.' }
        format.json { render :show, status: :created, location: @users_test }
      else
        format.html { render :new }
        format.json { render json: @users_test.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users_tests/1
  # PATCH/PUT /users_tests/1.json
  def update
    respond_to do |format|
      if @users_test.update(users_test_params)
        format.html { redirect_to @users_test, notice: 'Users test was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_test }
      else
        format.html { render :edit }
        format.json { render json: @users_test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users_tests/1
  # DELETE /users_tests/1.json
  def destroy
    @users_test.destroy
    respond_to do |format|
      format.html { redirect_to users_tests_url, notice: 'Users test was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_test
      @users_test = UsersTest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_test_params
      params.require(:users_test).permit(:users_id, :tests_id)
    end
end
