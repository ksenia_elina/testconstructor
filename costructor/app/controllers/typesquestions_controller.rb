class TypesquestionsController < ApplicationController
  before_action :set_typesquestion, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :require_admin, except:[:index]
  # GET /typesquestions
  # GET /typesquestions.json
  def index
    @typesquestions = Typesquestion.all
  end

  def all
    @typesquestions = Typesquestion.all
    render :json => @typesquestions 
  end
  # GET /typesquestions/1
  # GET /typesquestions/1.json
  def show
  end

  # GET /typesquestions/new
  def new
    @typesquestion = Typesquestion.new
  end

  # GET /typesquestions/1/edit
  def edit
  end

  # POST /typesquestions
  # POST /typesquestions.json
  def create
    @typesquestion = Typesquestion.new(typesquestion_params)

    respond_to do |format|
      if @typesquestion.save
        format.html { redirect_to @typesquestion, notice: 'Typesquestion was successfully created.' }
        format.json { render :show, status: :created, location: @typesquestion }
      else
        format.html { render :new }
        format.json { render json: @typesquestion.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /typesquestions/1
  # PATCH/PUT /typesquestions/1.json
  def update
    respond_to do |format|
      if @typesquestion.update(typesquestion_params)
        format.html { redirect_to @typesquestion, notice: 'Typesquestion was successfully updated.' }
        format.json { render :show, status: :ok, location: @typesquestion }
      else
        format.html { render :edit }
        format.json { render json: @typesquestion.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /typesquestions/1
  # DELETE /typesquestions/1.json
  def destroy
    @typesquestion.destroy
    respond_to do |format|
      format.html { redirect_to typesquestions_url, notice: 'Typesquestion was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_typesquestion
      @typesquestion = Typesquestion.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def typesquestion_params
      params.require(:typesquestion).permit(:name)
    end
end
