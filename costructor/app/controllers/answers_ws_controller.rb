class AnswersWsController < ApplicationController
  before_action :set_answers_w, only: [:show, :edit, :update, :destroy]
  before_action :require_admin, except:[:index, :show]
  # GET /answers_ws
  # GET /answers_ws.json
  def index
    @answers_ws = AnswersW.all
  end

  # GET /answers_ws/1
  # GET /answers_ws/1.json
  def show
  end

  # GET /answers_ws/new
  def new
    @answers_w = AnswersW.new
  end

  # GET /answers_ws/1/edit
  def edit
  end

  # POST /answers_ws
  # POST /answers_ws.json
  def create
    @answers_w = AnswersW.new(answers_w_params)

    respond_to do |format|
      if @answers_w.save
        format.html { redirect_to @answers_w, notice: 'Answers w was successfully created.' }
        format.json { render :show, status: :created, location: @answers_w }
      else
        format.html { render :new }
        format.json { render json: @answers_w.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /answers_ws/1
  # PATCH/PUT /answers_ws/1.json
  def update
    respond_to do |format|
      if @answers_w.update(answers_w_params)
        format.html { redirect_to @answers_w, notice: 'Answers w was successfully updated.' }
        format.json { render :show, status: :ok, location: @answers_w }
      else
        format.html { render :edit }
        format.json { render json: @answers_w.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /answers_ws/1
  # DELETE /answers_ws/1.json
  def destroy
    @answers_w.destroy
    respond_to do |format|
      format.html { redirect_to answers_ws_url, notice: 'Answers w was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_answers_w
      @answers_w = AnswersW.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def answers_w_params
      params.require(:answers_w).permit(:name, :right, :question_id)
    end
end
