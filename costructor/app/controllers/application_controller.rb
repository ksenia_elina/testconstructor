class ApplicationController < ActionController::Base
    def require_admin
        if !current_user || !current_user.admin?
              redirect_to root_url 
        end
    end
end
