class TestsController < ApplicationController
  before_action :set_test, only: [ :edit,:show, :update, :destroy, :showTestsToUser]
  #выше для джойна
  #before_action :authenticate_user!
 before_action :require_admin, except:[:index, :show]
  respond_to :html, :json, :xml


  # GET /tests
  # GET /tests.json
  def index
    if current_user.admin?
      @tests = Test.all
      else
        #@tests = Test.find()
        #@TestsUser = TestsUser.users_id(current_user.id) # => All active products whose names start with 'Ruby'
   
         #@TestsUser =  TestsUser.joins(:user).where(Users: { Users: Users })
         #@tests = Test.all
         #@tests2 = UsersTest.joins(:user,:test).where("users_id =" +current_user.id.to_s)
         @tests = UsersTest.select("users_tests.*, tests.*").joins(:user,:test).where("users_id =" +current_user.id.to_s)

         #render :json => @tests3

   
      end
   
   
    @types_test = TypesTest.all
    @id_of_course = Course.all
    @question = Question.all
  end

  def all
    @question = Question.all
    #format.json { render :json => @question }
    render :json => @question 
  end

  # GET /tests/1 
  # GET /tests/1.json
  def show
    @types_test = TypesTest.all
    @id_of_course = Course.all
    @questions = Question.where("test_id =" +@test.id.to_s)
     @answers = AnswersW.joins("JOIN questions ON answers_ws.question_id = questions.id ").where("test_id =" +@test.id.to_s)
  end
  
    def showTestsToUser
    @types_test = TypesTest.all
    @id_of_course = Course.all
    @questions = Question.where("test_id =" +@test.id.to_s)
     @answers = AnswersW.joins("JOIN questions ON answers_ws.question_id = questions.id ").where("test_id =" +@test.id.to_s)
     render 'showTestsToUser'
  end

  # GET /tests/new
  def new
    @test = Test.new
    @types_tests = TypesTest.all
    @id_of_course = Course.all
  @test.questions.build
  #@test.questions.answers_w.build
  @test.questions.each do |question|
    question.answers_ws.build
  end

    #@test.questions.each do |question|
     #   question.answers.build
    #end
  end

  # GET /tests/1/edit
  def edit
    @types_tests = TypesTest.all
    @id_of_course = Course.all
  end

  # POST /tests
  # POST /tests.json
  def create
    @test = Test.new(test_params)
    @types_tests = TypesTest.all
    @id_of_course = Course.all
    respond_to do |format|
      if @test.save
        format.html { redirect_to @test, notice: 'Test was successfully created.' }
        format.json { render :show, status: :created, location: @test }
      else
        format.html { render :new }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end

  end

  # PATCH/PUT /tests/1
  # PATCH/PUT /tests/1.json
  def update
    respond_to do |format|
      if @test.update(test_params)
        format.html { redirect_to @test, notice: 'Test was successfully updated.' }
        format.json { render :show, status: :ok, location: @test }
      else
        format.html { render :edit }
        format.json { render json: @test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tests/1
  # DELETE /tests/1.json
  def destroy
    @test.destroy
    respond_to do |format|
      format.html { redirect_to tests_url, notice: 'Test was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test
      @test = Test.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def test_params
      params.require(:test).permit(:name, :courses_id, :inf, :quest, :types_tests_id, questions_attributes: [:id,:name, :typesquestions_id, :variants_id, :test_id, 
      answers_ws_attributes:[:id, :name, :right]])
    end
end
