class AnswersTsController < ApplicationController
  before_action :set_answers_t, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :require_admin, except:[:index, :new, :create]
  # GET /answers_ts
  # GET /answers_ts.json
  def index
    @answers_ts = AnswersT.all
  end

  # GET /answers_ts/1
  # GET /answers_ts/1.json
  def show
  end

  # GET /answers_ts/new
  def new
    @answers_t = AnswersT.new
  end

  # GET /answers_ts/1/edit
  def edit
  end

  # POST /answers_ts
  # POST /answers_ts.json
  def create
    @answers_t = AnswersT.new(answers_t_params)

    respond_to do |format|
      if @answers_t.save
        format.html { redirect_to @answers_t, notice: 'Answers t was successfully created.' }
        format.json { render :show, status: :created, location: @answers_t }
      else
        format.html { render :new }
        format.json { render json: @answers_t.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /answers_ts/1
  # PATCH/PUT /answers_ts/1.json
  def update
    respond_to do |format|
      if @answers_t.update(answers_t_params)
        format.html { redirect_to @answers_t, notice: 'Answers t was successfully updated.' }
        format.json { render :show, status: :ok, location: @answers_t }
      else
        format.html { render :edit }
        format.json { render json: @answers_t.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /answers_ts/1
  # DELETE /answers_ts/1.json
  def destroy
    @answers_t.destroy
    respond_to do |format|
      format.html { redirect_to answers_ts_url, notice: 'Answers t was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_answers_t
      @answers_t = AnswersT.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def answers_t_params
      params.require(:answers_t).permit(:name, :right_t, :questions_id, :answers_ws_id, :tests_id, :users_id)
    end
end
