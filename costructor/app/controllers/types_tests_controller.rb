class TypesTestsController < ApplicationController
  before_action :set_types_test, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :require_admin
  # GET /types_tests
  # GET /types_tests.json
  def index
    @types_tests = TypesTest.all
  end

  # GET /types_tests/1
  # GET /types_tests/1.json
  def show
  end

  # GET /types_tests/new
  def new
    @types_test = TypesTest.new
  end

  # GET /types_tests/1/edit
  def edit
  end

  # POST /types_tests
  # POST /types_tests.json
  def create
    @types_test = TypesTest.new(types_test_params)

    respond_to do |format|
      if @types_test.save
        format.html { redirect_to @types_test, notice: 'Types test was successfully created.' }
        format.json { render :show, status: :created, location: @types_test }
      else
        format.html { render :new }
        format.json { render json: @types_test.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /types_tests/1
  # PATCH/PUT /types_tests/1.json
  def update
    respond_to do |format|
      if @types_test.update(types_test_params)
        format.html { redirect_to @types_test, notice: 'Types test was successfully updated.' }
        format.json { render :show, status: :ok, location: @types_test }
      else
        format.html { render :edit }
        format.json { render json: @types_test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /types_tests/1
  # DELETE /types_tests/1.json
  def destroy
    @types_test.destroy
    respond_to do |format|
      format.html { redirect_to types_tests_url, notice: 'Types test was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_types_test
      @types_test = TypesTest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def types_test_params
      params.require(:types_test).permit(:name)
    end
end
