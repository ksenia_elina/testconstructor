
function add_input(event){
    event.preventDefault()
    var new_id = $('#list input[type="text"]').length+1;
    console.log(new_id);
    setTimeout(formQuestion(new_id), 0); //для синхронности

};

function form(new_id)
{
    var input = document.createElement('INPUT');
    input.type = 'text';
    input.name = "test_questions_attributes_"+new_id+"_name";
    document.querySelector('#list').appendChild(input);
}

function formQuestion(new_id)
{
    CreateLabel('Вопрос', '#list')
    CreateLabel('name', '#list')
    CreateInput("test[questions_attributes]["+new_id+"][name]","test_questions_attributes_"+new_id+"_name", 'list')
    CreateLabel('type_of_question', '#list')
    GetSelector("test[questions_attributes]["+new_id+"][typesquestions_id]", "test_questions_attributes_"+new_id+"_typesquestions_id","/typesquestions/all");
    CreateLabel('variant', '#list')
    GetSelector("test[questions_attributes]["+new_id+"][variants_id]", "test_questions_attributes_"+new_id+"_variants_id", "/variants/all");
    var input = document.createElement('div');
    input.id = "sel"+new_id;
    document.querySelector('#list').appendChild(input);

    document.getElementById("list").innerHTML +='<input type="button" name="" onclick="add_inputAnswer('+new_id+');" value="Добавить ответ"/>';
}

function add_inputAnswer(new_id){
    var idA = $('#sel'+new_id+' input[type="text"]').length+1;
    console.log( );
    setTimeout(FormAnswer(new_id,idA, 'sel'+new_id), 0);

};

function FormAnswer(new_id,idA, sel)
{
    //alert(new_id);
    CreateLabel('Ответ #'+idA, '#'+sel)
    CreateLabel('name', '#'+sel)
    CreateInput("test[questions_attributes]["+new_id+"][answers_ws_attributes]["+idA+"][name]","test_questions_attributes_"+new_id+"_answers_ws_attributes_"+idA+"_name", sel)
    CreateLabel('right', '#'+sel)
    CreateInput("test[questions_attributes]["+new_id+"][answers_ws_attributes]["+idA+"][right]","test_questions_attributes_"+new_id+"_answers_ws_attributes_"+idA+"_right", sel, true)
}

function GetSelector(nameMain,id, address)
{
    var input = document.createElement('select');
    input.name = nameMain;
    input.id = id;
    document.querySelector('#list').appendChild(input);


    $.get(
        address,
        {
        },
        onAjaxSuccess
      );
       
      function onAjaxSuccess(data)
      {
        // Здесь мы получаем данные, отправленные сервером и выводим их на экран.
        data.forEach(elem => {
            // alert(elem.id);
             select = document.getElementById(id);
                 var opt = document.createElement('option');
                 opt.value = elem.id;
                 opt.innerHTML = elem.name;
                 select.appendChild(opt);
        });
       
      }
}

function CreateInput(nameMain,id, querySelector, ch = false)
{
    var input = document.createElement('INPUT');
    if (ch){
        input.type = 'checkbox';
    }
    else
    {
        input.type = 'text';
    }
    input.name = nameMain
    input.id = id;
    var sel = document.getElementById(querySelector);
    console.log(document.getElementById(querySelector));
    sel.appendChild(input);
}

function CreateLabel(nameMain, querySelector)
{
    var input = document.createElement('label');
    input.innerHTML = nameMain;
    document.querySelector(querySelector).appendChild(input);
}