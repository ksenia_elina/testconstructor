json.extract! question, :id, :name, :type_of_question, :created_at, :updated_at
json.url question_url(question, format: :json)
