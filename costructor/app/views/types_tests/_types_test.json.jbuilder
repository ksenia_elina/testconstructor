json.extract! types_test, :id, :name, :created_at, :updated_at
json.url types_test_url(types_test, format: :json)
