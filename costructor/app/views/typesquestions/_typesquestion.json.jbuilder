json.extract! typesquestion, :id, :name, :created_at, :updated_at
json.url typesquestion_url(typesquestion, format: :json)
