json.extract! answers_w, :id, :name, :right, :created_at, :updated_at
json.url answers_w_url(answers_w, format: :json)
