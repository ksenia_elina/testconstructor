json.extract! answers_t, :id, :name, :right_t, :created_at, :updated_at
json.url answers_t_url(answers_t, format: :json)
