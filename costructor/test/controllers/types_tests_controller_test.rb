require 'test_helper'

class TypesTestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @types_test = types_tests(:one)
  end

  test "should get index" do
    get types_tests_url
    assert_response :success
  end

  test "should get new" do
    get new_types_test_url
    assert_response :success
  end

  test "should create types_test" do
    assert_difference('TypesTest.count') do
      post types_tests_url, params: { types_test: { name: @types_test.name } }
    end

    assert_redirected_to types_test_url(TypesTest.last)
  end

  test "should show types_test" do
    get types_test_url(@types_test)
    assert_response :success
  end

  test "should get edit" do
    get edit_types_test_url(@types_test)
    assert_response :success
  end

  test "should update types_test" do
    patch types_test_url(@types_test), params: { types_test: { name: @types_test.name } }
    assert_redirected_to types_test_url(@types_test)
  end

  test "should destroy types_test" do
    assert_difference('TypesTest.count', -1) do
      delete types_test_url(@types_test)
    end

    assert_redirected_to types_tests_url
  end
end
