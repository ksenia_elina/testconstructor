require 'test_helper'

class AnswersTsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @answers_t = answers_ts(:one)
  end

  test "should get index" do
    get answers_ts_url
    assert_response :success
  end

  test "should get new" do
    get new_answers_t_url
    assert_response :success
  end

  test "should create answers_t" do
    assert_difference('AnswersT.count') do
      post answers_ts_url, params: { answers_t: { name: @answers_t.name, right_t: @answers_t.right_t } }
    end

    assert_redirected_to answers_t_url(AnswersT.last)
  end

  test "should show answers_t" do
    get answers_t_url(@answers_t)
    assert_response :success
  end

  test "should get edit" do
    get edit_answers_t_url(@answers_t)
    assert_response :success
  end

  test "should update answers_t" do
    patch answers_t_url(@answers_t), params: { answers_t: { name: @answers_t.name, right_t: @answers_t.right_t } }
    assert_redirected_to answers_t_url(@answers_t)
  end

  test "should destroy answers_t" do
    assert_difference('AnswersT.count', -1) do
      delete answers_t_url(@answers_t)
    end

    assert_redirected_to answers_ts_url
  end
end
