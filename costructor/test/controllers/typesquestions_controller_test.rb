require 'test_helper'

class TypesquestionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @typesquestion = typesquestions(:one)
  end

  test "should get index" do
    get typesquestions_url
    assert_response :success
  end

  test "should get new" do
    get new_typesquestion_url
    assert_response :success
  end

  test "should create typesquestion" do
    assert_difference('Typesquestion.count') do
      post typesquestions_url, params: { typesquestion: { name: @typesquestion.name } }
    end

    assert_redirected_to typesquestion_url(Typesquestion.last)
  end

  test "should show typesquestion" do
    get typesquestion_url(@typesquestion)
    assert_response :success
  end

  test "should get edit" do
    get edit_typesquestion_url(@typesquestion)
    assert_response :success
  end

  test "should update typesquestion" do
    patch typesquestion_url(@typesquestion), params: { typesquestion: { name: @typesquestion.name } }
    assert_redirected_to typesquestion_url(@typesquestion)
  end

  test "should destroy typesquestion" do
    assert_difference('Typesquestion.count', -1) do
      delete typesquestion_url(@typesquestion)
    end

    assert_redirected_to typesquestions_url
  end
end
