require 'test_helper'

class AnswersWsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @answers_w = answers_ws(:one)
  end

  test "should get index" do
    get answers_ws_url
    assert_response :success
  end

  test "should get new" do
    get new_answers_w_url
    assert_response :success
  end

  test "should create answers_w" do
    assert_difference('AnswersW.count') do
      post answers_ws_url, params: { answers_w: { name: @answers_w.name, right: @answers_w.right } }
    end

    assert_redirected_to answers_w_url(AnswersW.last)
  end

  test "should show answers_w" do
    get answers_w_url(@answers_w)
    assert_response :success
  end

  test "should get edit" do
    get edit_answers_w_url(@answers_w)
    assert_response :success
  end

  test "should update answers_w" do
    patch answers_w_url(@answers_w), params: { answers_w: { name: @answers_w.name, right: @answers_w.right } }
    assert_redirected_to answers_w_url(@answers_w)
  end

  test "should destroy answers_w" do
    assert_difference('AnswersW.count', -1) do
      delete answers_w_url(@answers_w)
    end

    assert_redirected_to answers_ws_url
  end
end
