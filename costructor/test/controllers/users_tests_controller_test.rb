require 'test_helper'

class UsersTestsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_test = users_tests(:one)
  end

  test "should get index" do
    get users_tests_url
    assert_response :success
  end

  test "should get new" do
    get new_users_test_url
    assert_response :success
  end

  test "should create users_test" do
    assert_difference('UsersTest.count') do
      post users_tests_url, params: { users_test: {  } }
    end

    assert_redirected_to users_test_url(UsersTest.last)
  end

  test "should show users_test" do
    get users_test_url(@users_test)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_test_url(@users_test)
    assert_response :success
  end

  test "should update users_test" do
    patch users_test_url(@users_test), params: { users_test: {  } }
    assert_redirected_to users_test_url(@users_test)
  end

  test "should destroy users_test" do
    assert_difference('UsersTest.count', -1) do
      delete users_test_url(@users_test)
    end

    assert_redirected_to users_tests_url
  end
end
