require "application_system_test_case"

class AnswersWsTest < ApplicationSystemTestCase
  setup do
    @answers_w = answers_ws(:one)
  end

  test "visiting the index" do
    visit answers_ws_url
    assert_selector "h1", text: "Answers Ws"
  end

  test "creating a Answers w" do
    visit answers_ws_url
    click_on "New Answers W"

    fill_in "Name", with: @answers_w.name
    fill_in "Right", with: @answers_w.right
    click_on "Create Answers w"

    assert_text "Answers w was successfully created"
    click_on "Back"
  end

  test "updating a Answers w" do
    visit answers_ws_url
    click_on "Edit", match: :first

    fill_in "Name", with: @answers_w.name
    fill_in "Right", with: @answers_w.right
    click_on "Update Answers w"

    assert_text "Answers w was successfully updated"
    click_on "Back"
  end

  test "destroying a Answers w" do
    visit answers_ws_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Answers w was successfully destroyed"
  end
end
