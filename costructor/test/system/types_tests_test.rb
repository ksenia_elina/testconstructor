require "application_system_test_case"

class TypesTestsTest < ApplicationSystemTestCase
  setup do
    @types_test = types_tests(:one)
  end

  test "visiting the index" do
    visit types_tests_url
    assert_selector "h1", text: "Types Tests"
  end

  test "creating a Types test" do
    visit types_tests_url
    click_on "New Types Test"

    fill_in "Name", with: @types_test.name
    click_on "Create Types test"

    assert_text "Types test was successfully created"
    click_on "Back"
  end

  test "updating a Types test" do
    visit types_tests_url
    click_on "Edit", match: :first

    fill_in "Name", with: @types_test.name
    click_on "Update Types test"

    assert_text "Types test was successfully updated"
    click_on "Back"
  end

  test "destroying a Types test" do
    visit types_tests_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Types test was successfully destroyed"
  end
end
