require "application_system_test_case"

class AnswersTsTest < ApplicationSystemTestCase
  setup do
    @answers_t = answers_ts(:one)
  end

  test "visiting the index" do
    visit answers_ts_url
    assert_selector "h1", text: "Answers Ts"
  end

  test "creating a Answers t" do
    visit answers_ts_url
    click_on "New Answers T"

    fill_in "Name", with: @answers_t.name
    fill_in "Right T", with: @answers_t.right_t
    click_on "Create Answers t"

    assert_text "Answers t was successfully created"
    click_on "Back"
  end

  test "updating a Answers t" do
    visit answers_ts_url
    click_on "Edit", match: :first

    fill_in "Name", with: @answers_t.name
    fill_in "Right T", with: @answers_t.right_t
    click_on "Update Answers t"

    assert_text "Answers t was successfully updated"
    click_on "Back"
  end

  test "destroying a Answers t" do
    visit answers_ts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Answers t was successfully destroyed"
  end
end
