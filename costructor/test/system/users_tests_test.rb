require "application_system_test_case"

class UsersTestsTest < ApplicationSystemTestCase
  setup do
    @users_test = users_tests(:one)
  end

  test "visiting the index" do
    visit users_tests_url
    assert_selector "h1", text: "Users Tests"
  end

  test "creating a Users test" do
    visit users_tests_url
    click_on "New Users Test"

    click_on "Create Users test"

    assert_text "Users test was successfully created"
    click_on "Back"
  end

  test "updating a Users test" do
    visit users_tests_url
    click_on "Edit", match: :first

    click_on "Update Users test"

    assert_text "Users test was successfully updated"
    click_on "Back"
  end

  test "destroying a Users test" do
    visit users_tests_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Users test was successfully destroyed"
  end
end
