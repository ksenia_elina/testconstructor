require "application_system_test_case"

class TypesquestionsTest < ApplicationSystemTestCase
  setup do
    @typesquestion = typesquestions(:one)
  end

  test "visiting the index" do
    visit typesquestions_url
    assert_selector "h1", text: "Typesquestions"
  end

  test "creating a Typesquestion" do
    visit typesquestions_url
    click_on "New Typesquestion"

    fill_in "Name", with: @typesquestion.name
    click_on "Create Typesquestion"

    assert_text "Typesquestion was successfully created"
    click_on "Back"
  end

  test "updating a Typesquestion" do
    visit typesquestions_url
    click_on "Edit", match: :first

    fill_in "Name", with: @typesquestion.name
    click_on "Update Typesquestion"

    assert_text "Typesquestion was successfully updated"
    click_on "Back"
  end

  test "destroying a Typesquestion" do
    visit typesquestions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Typesquestion was successfully destroyed"
  end
end
