Rails.application.routes.draw do
  devise_for :users
  resources :registrations, :controller => 'users'
  get 'persons/profile'
  resources :roles
  resources :users_tests
  resources :users
  resources :variants do
  collection do
    get 'all'
  end
end
  resources :answers_ts
  resources :answers_ws
  get 'main/Index'
  resources :typesquestions do
  collection do
    get 'all'
  end
end
  resources :questions
  resources :types_tests
  resources :courses

  resources :tests do
    collection do
      get 'all'
    end
  end
  
  resources :showTestsToUser , :controller => 'tests', :action => 'showTestsToUser'

  root 'main#Index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
